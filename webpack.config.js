const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')
// const WorkboxPlugin = require('workbox-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin')

const rulesForStyles = {
  test: /\.css$/,
  use: ['style-loader', 'css-loader'],
}

const rulesForSasStyles = {
  test: /\.(s(a|c)ss)$/,
  use: ['style-loader', 'css-loader', 'sass-loader'],
}

const rulesForJavascript = {
  test: /\.(js|jsx)?$/,
  exclude: /node_modules/,
  loader: 'babel-loader',
  options: {
    presets: [
      '@babel/preset-env',
      ['@babel/preset-react', { runtime: 'automatic' }],
      {
        plugins: ['@babel/plugin-transform-runtime'],
      },
    ],
  },
}

const rules = [rulesForJavascript, rulesForStyles, rulesForSasStyles]

module.exports = (env, argv) => {
  const { mode } = argv
  const isProduction = mode === 'production'

  return {
    entry: './src/main.js',
    devtool: 'inline-source-map',
    output: {
      filename: isProduction ? '[name].[contenthash].js' : 'main.js',
      path: path.resolve(__dirname, 'build'),
      publicPath: '/',
    },
    devtool: 'inline-source-map',
    devServer: {
      open: true,
      compress: true,
      port: 3000,
      static: './build',
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
      // new WorkboxPlugin.GenerateSW({
      //   clientsClaim: true,
      //   skipWaiting: true
      // }),
      // new CopyWebpackPlugin({
      //   patterns: [
      //     { from: 'public', to: '' }
      //   ]}
      // ),
      new Dotenv(),
    ],
    module: {
      rules: rules,
    },
    resolve: {
      extensions: ['jsx', '.ts', '.js'],
    },
  }
}
