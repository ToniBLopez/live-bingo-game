const { getRandomNumber } = require('./utils')

const connectedPlayers = new Set()

const statusTypes = {
  lobby: 1,
  inProgress: 2,
  scoreBoard: 3,
}

const socketEvents = (io) => {

let game = false

  io.on('connection', (socket) => {

    const restartGame = () => {
      game = {
        status: statusTypes.lobby,
        balls: [],
        lineWinner: null,
        bingoWinner: null,
      }
      io.emit('game', game)
    }

    if (connectedPlayers.size < 1 || !game) {
      restartGame()
    }

    // *** On player connection
    // Add the player to the connected players set if the game is in lobby
    if (game.status === statusTypes.lobby) {
      // Emit the game object to the new player
      socket.emit('game', game)
    } else {
      // Let the client know that there's a game in progress, and he can't join
      socket.emit('game', false)
    }

    // *** Socket events listeners
    // Send the current socket id back to the client
    socket.on('getId', (res) => {
      res(socket.id)
    })
    socket.on('getConnectedPlayers', (res) => {
      res([...connectedPlayers])
    })

    // A client will ask to joinGame when on lobby
    socket.on('joinGame', () => {
      // Add the player to the connected players set
      connectedPlayers.add(socket.id)
      // Add the player to the socket room 'isPlaying'
      socket.join('isPlaying')
      // Send the current players to all clients
      io.to('isPlaying').emit('connectedPlayers', [...connectedPlayers])
    })

    socket.on('gameStart', () => {
      game.status = statusTypes.inProgress
      io.to('isPlaying').emit('game', game)
    })

    socket.on('line', () => {
      // If is the first line in the game, set the game lineWinner and emit the game object to all players
      if (!game.lineWinner) {
        game.lineWinner = socket.id
        io.to('isPlaying').emit('game', game)
      }
    })

    // Listen get new ball and I emit'em to all players:
    socket.on('newBall', () => {
      const newBall = getRandomNumber(game.balls)
      game.balls.push(newBall)
      io.to('isPlaying').emit('ball', newBall)
    })

    socket.on('bingo', () => {
      game.bingoWinner = socket.id
      game.status = statusTypes.scoreBoard
      io.to('isPlaying').emit('game', game)
    })

    socket.on('gameEnd', () => {
      // reset the game object
      restartGame()
    })

    socket.on('disconnect', () => {
      connectedPlayers.delete(socket.id)
      console.log(`Socket ${socket.id} disconnected`)
      io.to('isPlaying').emit('connectedPlayers', [...connectedPlayers])
    })
  })
}

module.exports = socketEvents
