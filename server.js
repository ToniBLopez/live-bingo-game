require('dotenv').config() // This allows us to use const setted in the .env file
const { getRandomNumber } = require('./utils')

const express = require('express')
const path = require('path')
const app = express()
const server = require('http').createServer(app)

// Socket events
const io = require('socket.io')(server)
const socketEvents = require('./socketEvents') // This is the file that contains the socket.io logic
socketEvents(io) 

// *** Define your express endpoints and logic  ***

app.get('/get_board', (req, res) => {
  const boardNumbers = []
  for (i = 0; i < 25; i++) {
    const randomNumber = getRandomNumber(boardNumbers)
    boardNumbers.push(randomNumber)
  }
  res.json({ board_numbers: boardNumbers })
})

// All other GET requests to our app will go to our React app
app.use(express.static(path.join(__dirname, 'build')));
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
});

const port = process.env.PORT || 5000
server.listen(port, () => {
  console.log(`server listening on port ${port}`)
})

