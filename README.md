# Onetap developer test

> Build a bingo game :stuck_out_tongue_winking_eye:

You'll find in this repository a boilerplate to start the project. 
It already contains a basic file structure, as well as most of the dependencies you'll need to complete the exercise. But don't be attached to it, if you want, you can add any tool/lib that may help you or that you're more comfortable working with (keeping in mind you mainly have to focus on react and express). 

***

## Phase 1: Express.js & React.js

### What you need to build in this phase 
We're asking you to build a bingo game. 
The board will be a 5x5 grid, being the center cell a wild card, that represents all numbers.

### Backend requirements
Working on the server.js file:

There's a 'balls' variable, where we will store all the balls with random numbers that have been appearing in the game.

- We need a function that generates each ball number, making sure that has not been used, comparing with the 'balls' variable. (Random number from 1 to 100).

- We need a function that generates the numbers of a board game. (An array of 24 random unique numbers from 1 to 100)

The express server needs two endpoints:

- GET: /get_board (will respond with a new board for the player, and resets the 'balls' variable to an empty array)

- GET: /new_ball (will respond with a new ball number)

### Frontend requirements
Working on the react app (src/App.js):

- We'll need to have persistence on the board numbers and the appeared played balls. (local storage is perfect for this). Remember to empty the stored values on the client when it's necessary.

- On mounting the app, check if we have already a board stored on the client, if not fetch the /get_board end point to get one. Set the result in a state variable.
(Tip: You need to use the hooks useEffect, useState)

- Also define a state variable where to store all the ball numbers played. It will initiate with the value stored on the client, or an empty array if there's no one. 

- Build two buttons, one that gets a new board, and one that gets a new ball. In both cases making use of the server to get the result. 

- Build a Board component passing both the board numbers and balls appeared, through it's props. This board component has to check, when is needed, if there's a line or a bingo on the board. Display a message if it's the case. 


#### Styling requirements
There's no styling requirements, just make it all usable.

***

## Phase 2: Adding functionality and real time communication (socket.io)

### What you need to build in this phase
For having real fun with this bingo game, we need to make it multiplayer!

The game has now three diferent views:

Lobby: With no game ongoing. This will be a lobby where we can wait until our friends are connected, and we have a button to start a game. 

Playing: When the game starts, we get the bingo we build on the first phase, but also with a list with all the players on the game. Each player has his own board numbers, but any time one asks for a new ball, this ball is played for all.

Bingo: When the game has a bingo, we see a scoreboard, that makes us notice which players did the bingo (winner), and the first line (second price). In this scoreboard we have a button to end the game, what restore the app into an initial state.

Those views live all within the same pa

### Backend requirements

#### Changes
- Manage the serving of the played balls through socket, instead of http requests, so all clients can get the ball at the moment somene plays it. Notice that now we need to restart the played balls on the server when a game starts, instead of on board request.

- Now we don't have to reset the played balls on new board request, instead we need to define a way to inicialize a game

#### New stuff
- Watch the connected players into an Set type array, storing their's socket id. 
- Build all the 'socket endpoints'for having all clients allways updated with the player list, and if they have line or bingo, and all needed to meet the game requirements.


### Frontend requirements

#### Changes
- We now have to fetch the played balls, and ask new balls all throug socket communication instead of http request.

#### New stuff
- Create a lobby component, where you can visualize the players connected as well as you can start the game. This component has to be shown when there's no game ongoing. 
- Create a new players component that will display all the connected players, and will visualize when someone has line. This component will be shown while playing the bingo game.
- Create a end game component, where you see the bingo winners (1st line and bingo players), as well as you can end the game.

### Styling requirements
There's no styling requirements, just make it all usable.

***

# First steps

### Clone this repository to your local machine
`git clone https://gitlab.com/onetap1/onetap-dev-test.git`

> Make sure you are in the project folder when executing next commands.

### Create a new local git branch where you will do your work
`git checkout -b {{ name_of_your_branch }}` 

### Set up an upstream branch on the remote repository for your new local branch
`git push --set-upstream origin {{ name_of_your_branch }}`

### Rename .env.demo 
You need to rename .env.demo in the root folder to .env

### Install npm dependencies
`npm i`

### Run your development environment
`npm run dev`

This will start both the express server and the react app in watch mode, so the server will automatically refresh on any code/file change and serves the react app into [localhost:5000](http://localhost:5000).


### Happy coding! :relaxed:
Have fun building stuff 

### Remember to push the final code to the remote branch
`git push`