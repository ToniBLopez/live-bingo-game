const getRandomNumber = (prevNumbers = [], min = 1, max = 100) => {
    // If the array is full, return
  if (1 + max - min === prevNumbers.length) return
  
  let randomNumber = false
  while (!randomNumber) {
    const number = Math.floor(Math.random() * (max - min + 1)) + min
    if (!prevNumbers.includes(number)) {
      randomNumber = number
    }
  }
  return randomNumber
}

module.exports = {
  getRandomNumber
}
