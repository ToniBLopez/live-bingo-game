import { socket } from "./utils/socketConnection"

import './App.scss'
import Lobby from './pages/Lobby'
import BingoGame from './pages/BingoGame/BingoGame'
import ScoreBoard from './pages/ScoreBoard'
import CantJoin from './pages/CantJoin'

import { useState, useEffect } from 'react';

const statusTypes = {
  lobby: 1,
  inProgress: 2,
  scoreBoard: 3,
}

const initalGame = {
  status: null,
  balls: [],
  lineWinner: null,
  bingoWinner: null,
}

const App = () => {

  const [game, setGame] = useState(initalGame)

  useEffect(() => {
    socket.on('game', data => setGame(data))
  }, [])

  return (
    <div>
      {
        game && game.status === statusTypes.lobby &&
        <Lobby />
      }
      {
        game && game.status === statusTypes.inProgress &&
        <BingoGame  game={game}/>
      }
      {
        game && game.status === statusTypes.scoreBoard &&
        <ScoreBoard game={game}/>
      }
      {
        !game && <CantJoin />
      }
    </div>
  )
}

export default App