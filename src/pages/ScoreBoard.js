
import { socket } from '../utils/socketConnection'
import useSocketId from '../hooks/useSocketId'
import './ScoreBoard.scss'

function ScoreBoard({ game }) {

  const { socketId } = useSocketId()
  const { lineWinner, bingoWinner } = game

  return (
    <div>
      <div className='tittleContainer'>
        <h1 id='scoreTittle'>score</h1>
      </div>

      <div className='scoreDataContainer'>
        <div className='scoreDataBoard'>
          <div className='scoreBingoPlayer'>
            {`Bingo made by: ${socketId === bingoWinner ? 'You' : bingoWinner}`}
          </div>
          <div className='firstLinePlayer'>
          {`First line made by: ${socketId === lineWinner ? 'You' : lineWinner}`}
          </div>
        </div>
      </div>

      <div className='buttonContainer endGameButtonContainer'>
        <div type="submit" className='endTheGameButton' onClick={() => socket.emit('gameEnd')}>
          End The Game
        </div>
      </div>
    </div>
  )
}

export default ScoreBoard