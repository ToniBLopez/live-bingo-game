
import { socket } from "../../utils/socketConnection"
import React, { useState, useEffect } from 'react'
import Board from './components/Board';
import ConnectedPlayers from '../../components/ConnectedPlayers';

import './BingoGame.scss'

function BingoGame({ game }) {

  const [ball, setBall] = useState();
  const [board, setBoard] = useState([]);
  const [playedNumbers, setPlayedNumbers] = useState([]);

  useEffect(() => {
    getBoard()
    socket.on('ball', data => setBall(data))
  }, [])

  useEffect(() => {
    if (!ball && playedNumbers.length > 0) {
      setBall(playedNumbers[playedNumbers.length - 1])
    } else if (playedNumbers[playedNumbers.length - 1] === undefined) {
      setBall(undefined)
    }
  }, [playedNumbers])

  useEffect(() => {
    if (ball && playedNumbers[playedNumbers.length - 1] !== ball) {
      setPlayedNumbers([...playedNumbers, ball])
    }
  }, [ball])


  function getBoard() {
    const url = 'http://localhost:5000/get_board'
    fetch(url)
      .then(res => res)
      .then(async res => {
        const data = await res.json()
        setBoard(data.board_numbers)
      })
      .catch(err => console.error(err))
  }



  return (
    <>
      <main>
        <div className='tittleContainer'>
          <h1 id='bingoTittle'>bingo</h1>
        </div>

        <div className='dataContainer'>

          <div className='playersSection'>
            <h3 className='tittlePlayers'>Connected Players</h3>
            <ConnectedPlayers lineWinner={game.lineWinner} />
          </div>
          <div className='infoSection'>
            <div className='infoCard'>
              <img src='https://img.icons8.com/metro/208/F1C40F/about.png' className='logo' alt='Icon info image' />
            </div>
            <div className='infoCoincidence'>
              <div className='color' />
              <div className='text'>
                <h4>Coincidence</h4>
              </div>
            </div>
            <div className='infoLine'>
              <div className='color' />
              <div className='text'>
                <h4>Line</h4>
              </div>
            </div>
            <div className='infoBingo'>
              <div className='color' />
              <div className='text'>
                <h4>Bingo</h4>
              </div>
            </div>
          </div>
        </div>

        <Board socket={socket} board={board} ball={ball} playedNumbers={playedNumbers}/>

        <div className='buttonContainer newBallButtonContainer'>
          <div type="submit" className='ballButton' onClick={() => socket.emit('newBall')}>
            New Ball
          </div>
        </div>
      </main>
    </>

  )

}

export default BingoGame