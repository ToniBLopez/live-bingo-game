
import React, { useEffect, useState } from 'react';


const BoardLine = ({ socket, lineNumbers, playedNumbers, bingo }) => {

  const [isBingoLine, setIsBingoLine] = useState(false)


  useEffect(() => {
    if (playedNumbers.length > 0) {
      const bingoLine = lineNumbers.every(number => playedNumbers.includes(number))
      setIsBingoLine(bingoLine)
    }
  }, [playedNumbers])

  useEffect(() => {
    if(isBingoLine) {
      socket.emit('line')
    }
  }, [isBingoLine])


  return (
    <>
      {
        lineNumbers.map(number => {
          return (
            <BoardNumber
              socket={socket}
              number={number}
              bingoLine={isBingoLine}
              bingo={bingo}
              playedNumbers={playedNumbers}
            />
          )
        })
      }
    </>
  )

}


const BoardNumber = ({
  socket,
  number,
  bingoLine,
  bingo,
  playedNumbers
}) => {

  const [coincidence, setCoincidence] = useState(false)
  const [onlyOneEmit, setOnlyOneEmit] = useState(false)


  useEffect(() => {
    if (playedNumbers) {
      const isCoincidence = playedNumbers.includes(number)
      setCoincidence(isCoincidence)
    }
  }, [playedNumbers])


  function line () {
    if (!onlyOneEmit) {
      socket.emit('line')
      setOnlyOneEmit(true)
    }
    return 'line'
  }


    return (
      <div className={`box ${ bingo ? 'bingo' : bingoLine ? line() : coincidence ? 'coincidence' : 'clean' }`}>
        { number }
      </div>
    )

}


const BallNumber = ({ boardNumbers, actualBall }) => {

  const checkNumber = boardNumbers.includes(actualBall)

    return (
      <div className={`ballNumber ${ checkNumber ? 'coincidence' : 'clean' }`}>
        { actualBall ? actualBall : 'X' }
      </div>
    )

}


const Board = ({ socket, board, ball, playedNumbers }) => {

  const [bingo, setBingo] = useState(false)

  const lines = [0, 1, 2, 3, 4]


  useEffect(() => { // Check Bingo
    if (playedNumbers.length > 0) {
      const isBingo = board.every(number => playedNumbers.includes(number))
      setBingo(isBingo)
    }
  }, [playedNumbers])

  useEffect(() => {
    if(bingo) {
      socket.emit('bingo')
      socket.emit('newGameStatus', 'score')
    }
  }, [bingo])


  return (
    <>
      <div className='sectionBoard'>
        <div className='boardContainer'>
          {
            lines.map((line, index) => {
              return <BoardLine
                socket={socket}
                lineNumbers={board.slice(index * 5, (index * 5) + 5)}
                playedNumbers={playedNumbers}
                bingo={bingo}
              />
            })
          }
        </div>
      </div>

      <div className='ballCard'>
        <div className='cardText'>
          <h4>Ball:</h4>
        </div>
        <BallNumber boardNumbers={board} actualBall={ball} playedNumbers={playedNumbers}/>
      </div>
    </>
  )

}

export default Board