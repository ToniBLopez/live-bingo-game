const CantJoin = () => {

  return (
    <div>
      Currently there's a game in progress. Please wait until it's over.
    </div>
  )

}

export default CantJoin