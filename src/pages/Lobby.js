import { socket } from '../utils/socketConnection'
import { useEffect } from 'react'

import ConnectedPlayers from '../components/ConnectedPlayers'

import './Lobby.scss'

function Lobby() {

  useEffect(() => {
    socket.emit('joinGame')
  }, [])

  return (
    <div className='lobbyScreenContainer'>

      <div className='tittleContainer'>
        <h1 id='lobbyTittle'>lobby</h1>
      </div>

      <div className='mainLobbyContainer'>
        <ConnectedPlayers />

        <div className='buttonContainer lobbyScreenButtonContainer'>
          <div type="submit" className='startGameButton' onClick={() => socket.emit('gameStart')}> {/* Emit the new status */}
            Start Game
          </div>
        </div>
      </div>

    </div>
  )
}

export default Lobby