import { socket } from "../utils/socketConnection"
import { useState, useEffect } from "react"
import useSocketId from "../hooks/useSocketId"

import './ConnectedPlayers.scss'

const ConnectedPlayers = ({ lineWinner }) => {

  const [connectedPlayers, setConnectedPlayers] = useState([])
  const { socketId } = useSocketId()

  useEffect(() => {
    // We ask the server to send us the list of connected players
    socket.emit('getConnectedPlayers', players => setConnectedPlayers(players))
    // We listen to changes on player list from the server
    socket.on('connectedPlayers', data => setConnectedPlayers(data))
  }, [])

  return (
    <div className='ConnectedPlayers'>
    {
      connectedPlayers &&
      connectedPlayers.map((playerId, index) => (
        <div key={index} className='player'>
          {
            lineWinner === playerId && <strong>first line!</strong>
          }
          {
            playerId === socketId 
            ? `You are connected (id: ${playerId})`
            : `${playerId} is connected`
          }
        </div>
      ))
    }
  </div>
  )
}

export default ConnectedPlayers