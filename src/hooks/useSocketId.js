import { socket } from "../utils/socketConnection"
import { useState, useEffect } from 'react'

const useSocketId = () => {
  const [socketId, setSocketId] = useState('')

  useEffect(() => {
    socket.emit('getId', id => setSocketId(id))
  }, [])

  return { socketId }

}

export default useSocketId